#!/bin/bash

#====================================================#
# Default Variables                                  #
#====================================================#
RandomString='TestToElevatedCommandPrompt'
HostPath='/mnt/c/Windows/System32/drivers/etc/hosts'
WebRoot='/var/www/'
sitesEnabled='/etc/apache2/sites-enabled/'
sitesAvailable='/etc/apache2/sites-available/'
DBPassword='123'
DBUser='wp'

#color_variables
red="\e[31m"
green="\e[32m"
yellow="\e[33m"
blue="\e[34m"
cyan="\e[96m"
white="\e[39m"
magenta="\e[95m"

echo -e "$cyan===================================="
echo -e "$cyan= Teqnius Wordpress Install Script ="
echo -e "====================================$white"

#====================================================#
# 1. Check for Root(WSL) and Admin(Win) Privileges   #
#====================================================#
# Check if we have windows Adminstrative Privilages to Write to hosts file
# (/mnt/c/Windows/System32/drivers/etc/hosts)
# Here we simply add a random string and delete it

    echo -e "\n\n$magenta 1. Checking for root(WSL) and Administrator(Windows) Privileges..."
    if ! echo "$RandomString" >> $HostPath
    then
        echo -e "\n\n$red ❌ Failed: Admin Check!$white You don't have Administrative privileges on Windows. Please run as administrator. Exiting now..."
        exit 1
    else
        sed -i "/$RandomString/d" $HostPath
        echo -e "\n\n$green ✅ Passed: Admin Check!$white You have Administrative privileges on Windows. Moving on..."
    fi

    #Check if Root
    if [ "$(whoami)" != 'root' ]; then
        echo -e "\n\n$red ❌ Failed: Root Check!$white You don't have Root privileges on WSL. Please use sudo. Exiting now..."
        exit 1;
    else

        echo -e "\n\n$green ✅ Passed: Root Check!$white Moving on..."
    fi


#====================================================#
# 2. Get Project Name and Run Tests                  #
#====================================================#

#2.1 Get Project Name

    echo -e "\n\n$magenta 2. Getting Project Name and Checking Availability...\n$white"
    echo -e "\n\n$yellow Please enter your Project Name:$white"
    echo -e "(Make Sure its a unique name and it is not previously used on this machine for any other project)"
    read ProjectName

#2.2 Check if Directory Exists

    if [ -d "$WebRoot$ProjectName" ]; then
        ### Take action if directory with project name exists ###
        echo -e "$red ❌ Failed: Directory Check !$white Directory with the name ${ProjectName} Already Exists in ${WebRoot}, Please Choose Another Prject Name! Exiting now..."
        exit 1
    else
        echo -e "\n\n$green ✅ Passed:Directory Check!$white Moving on..."
    fi

#2.3 Check if DB Exists

    DBStatus=$(sudo mysql -u$DBUser -p$DBPassword -s -N -e "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='$ProjectName'")
    if [ -z "$DBStatus" ]; then
        echo -e "\n\n$green✅ Passed:DB Check!$white Moving on ..."
    else
        echo -e "\n\n$red❌ Failed: DB Check!$white DB with the name "${ProjectName}" already exists, Please Choose Another Prject Name! Exiting now..."
        exit 1
    fi

#2.4 Check if vhost for the project name exists already

    ProjectDomain=$ProjectName.local
    sitesAvailabledomain=$sitesAvailable$ProjectDomain.conf
    if [ -e $sitesAvailabledomain ]; then
        echo -e "\n\n$red❌ Failed: VHost Check!$white Error:The domain name: ${ProjectName} already exists, Please Choose Another Prject Name! Exiting now..."
        exit 1
    else
        echo -e "\n\n$green✅ Passed: VHost Check!$white Moving on..."
    fi

#====================================================#
# 3. Start Creating                                  #
#====================================================#

#By Now Everything required to create a new project is ready. Let's create a new project
echo -e "\n\n$magenta 3. Creating a new WordPress instance...\n$white"


#3.1 Create Directory and Change Permissions

    echo -e "\n\n$magenta 3.1 Creating project directory and changing permissions...\n$white"
    ProjectRoot=$WebRoot$ProjectName

    mkdir $ProjectRoot
    ### Give permission to Project Root
    chmod 755 $ProjectRoot
    ### write test file in the new domain dir
    if ! echo "<?php echo phpinfo(); ?>" > $ProjectRoot/phpinfo.php
    then
        echo -e "\n$red ❌ ERROR:$white Not able to write to the file $ProjectRoot/phpinfo.php. Please check permissions"
        exit 1;
    else
        echo -e "\n$green ✅ Success!$white Added test content to $ProjectRoot/phpinfo.php"
    fi

#3.2 Generate SSL Certificate for $ProjectDomain

    echo -e "\n\n$magenta 3.2 Generating SSL Certificate for $ProjectDomain ...\n$white"
    # SSL Variables, refer to lamp setup script
    cert_path_wsl='/etc/apache2/ssl_cert'
    ca_name='teqnius' #no spaces allowed
    org_name='Teqnius Business Solutions'
    dept='DevOps'
    email='ali@teqnius.com'
    country='IN'
    state='Kerala'
    city='Calicut'
    pass='1234'

    #Generate Private Key
    openssl genrsa -out $cert_path_wsl/$ProjectDomain.key 2048

    #Generate CSR
    openssl req -new -key $cert_path_wsl/$ProjectDomain.key -out $cert_path_wsl/$ProjectDomain.csr -passin pass:1234 \
    -subj "/C=$country/ST=$state/L=$city/O=$org_name/OU=$dept/CN=$org_name/emailAddress=$email"

    #Create SSL Configuration File
    touch $cert_path_wsl/sslconf.ext
    sudo echo -e "authorityKeyIdentifier=keyid,issuer
    basicConstraints=CA:FALSE
    keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
    subjectAltName = @alt_names

    [alt_names]
    DNS.1 = $ProjectDomain" > "$cert_path_wsl/sslconf.ext"

    #Generate SSL Certificate using Root Certificate and Private Key
    openssl x509 -req -in $cert_path_wsl/$ProjectDomain.csr -CA $cert_path_wsl/$ca_name.pem -CAkey $cert_path_wsl/$ca_name.key -CAcreateserial \
    -out $cert_path_wsl/$ProjectDomain.crt -days 825 -sha256 -extfile $cert_path_wsl/sslconf.ext -passin pass:1234

    # exit #toberemoved
#3.3 Create VHost
    echo -e "\n\n$magenta 3.3 Creating VHost for $ProjectDomain and installing SSL Certificate...\n$white"

    if ! echo "
    <VirtualHost *:80>
        ServerAdmin admin@$ProjectDomain
        ServerName $ProjectDomain
        ServerAlias $ProjectDomain
        DocumentRoot $ProjectRoot
        <Directory />
            AllowOverride All
        </Directory>
        <Directory $ProjectRoot>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride all
            Require all granted
        </Directory>
        ErrorLog /var/log/apache2/$ProjectDomain-error.log
        LogLevel error
        CustomLog /var/log/apache2/$ProjectDomain-access.log combined
    </VirtualHost>
    <IfModule mod_ssl.c>
        <VirtualHost _default_:443>
            ServerAdmin admin@$ProjectDomain
            ServerName $ProjectDomain
            ServerAlias $ProjectDomain
            DocumentRoot $ProjectRoot
            <Directory />
                AllowOverride All
            </Directory>
            <Directory $ProjectRoot>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride all
                Require all granted
            </Directory>
            ErrorLog /var/log/apache2/error.log
            CustomLog /var/log/apache2/access.log combined
            SSLEngine on
            SSLCertificateFile $cert_path_wsl/$ProjectDomain.crt
            SSLCertificateKeyFile $cert_path_wsl/$ProjectDomain.key
        </VirtualHost>
    </IfModule>" > $sitesAvailabledomain
    then
        echo -e "$red Failed!$white There is an ERROR creating $ProjectDomain VHost file"
        exit 1;
    else
        echo -e $"\nNew Virtual Host Created\n"
    fi
    #End Create VHost

#3.4 Add Hosts Entry
    echo -e "\n\n$magenta 3.4 Creating Host File Entry for $ProjectDomain\n$white"

    ### Add domain in /etc/hosts
    if ! echo "127.0.0.1	$ProjectDomain" >> /etc/hosts
    then
        echo -e "$red ERROR:$white Not able to write in /etc/hosts"
        exit;
    else
        echo -e "$green SUCCESS:$white Host added to /etc/hosts file \n"
    fi

    ### Add domain in /mnt/c/Windows/System32/drivers/etc/hosts (Windows Subsytem for Linux)
    if [ -e /mnt/c/Windows/System32/drivers/etc/hosts ]
    then
        # if ! echo -e "127.0.0.1 $ProjectDomain\n::1 $ProjectDomain" >> /mnt/c/Windows/System32/drivers/etc/hosts #https://github.com/microsoft/WSL/issues/4347#issuecomment-516093299
        if ! echo -e "::1 $ProjectDomain localhost" >> /mnt/c/Windows/System32/drivers/etc/hosts #https://github.com/microsoft/WSL/issues/4347#issuecomment-567968346
        then
            echo "$red ERROR:$white Not able to write in /mnt/c/Windows/System32/drivers/etc/hosts (Hint: Try running Bash as administrator)"
        else
            echo -e $"$green SUCCESS:$white Host added to /mnt/c/Windows/System32/drivers/etc/hosts file \n"
        fi
    fi

#3.5 Add Permissions and Enabling $ProjectDomain
    echo -e "\n\n$magenta 3.5 Assigning Permissions and Enabling $ProjectDomain...\n$white"
    if [ "$owner" == "" ]; then
        iam=$(whoami)
        if [ "$iam" == "root" ]; then
            chown -R $apacheUser:$apacheUser $ProjectRoot
        else
            chown -R $iam:$iam $ProjectRoot
        fi
    else
        chown -R $owner:$owner $ProjectRoot
    fi

    # enable website
    a2ensite $ProjectDomain

#3.6 Restart Apache and test Run
    echo -e "\n\n$magenta 3.6 Restarting Apache and Test Running$white"
    sudo service apache2 restart
    sudo service mysql restart
    xdg-open "http://$ProjectDomain/phpinfo.php"
    xdg-open "https://$ProjectDomain/phpinfo.php"


#====================================================#
# 4. Install WordPress                               #
#====================================================#
cd $ProjectRoot
run="y"
setupmysql="y"
harden="n"
mysqlhost="localhost"
dbtable="wp_"
hardenkey="teq34126"
if [ "$run" == y ] ; then
	if [ "$setupmysql" == y ] ; then
		echo "============================================"
		echo "Setting up the database."
		echo "============================================"
		#login to MySQL, add database, add user and grant permissions
		# dbsetup="create database $ProjectName;GRANT ALL PRIVILEGES ON $ProjectName.* TO $DBUser IDENTIFIED BY '$DBPassword';FLUSH PRIVILEGES;"
		dbsetup="create database $ProjectName;"
		mysql -u $DBUser -p$DBPassword -e "$dbsetup"
		if [ $? != "0" ]; then
			echo "============================================"
			echo "[Error]: Database creation failed. Aborting."
			echo "============================================"
			exit 1
		fi
	fi
	echo "============================================"
	echo "Installing WordPress for you."
	echo "============================================"
	#download wordpress
	echo "Downloading..."
	curl -O https://wordpress.org/latest.tar.gz
	#unzip wordpress
	echo "Unpacking..."
	tar -zxf latest.tar.gz
	#move /wordpress/* files to this dir
	echo "Moving..."
	mv wordpress/* ./
	echo "Configuring..."
	#create wp config
	mv wp-config-sample.php wp-config.php
	#set database details with perl find and replace
	perl -pi -e "s'database_name_here'"$ProjectName"'g" wp-config.php
	perl -pi -e "s'username_here'"$DBUser"'g" wp-config.php
	perl -pi -e "s'password_here'"$DBPassword"'g" wp-config.php
	perl -pi -e "s/\'wp_\'/\'$dbtable\'/g" wp-config.php
	#set WP salts
	perl -i -pe'
	  BEGIN {
	    @chars = ("a" .. "z", "A" .. "Z", 0 .. 9);
	    push @chars, split //, "!@#$%^&*()-_ []{}<>~\`+=,.;:/?|";
	    sub salt { join "", map $chars[ rand @chars ], 1 .. 64 }
	  }
	  s/put your unique phrase here/salt()/ge
	' wp-config.php
	#create uploads folder and set permissions
	mkdir wp-content/uploads
	chmod 775 wp-content/uploads
	if [ "$harden" == y ] ; then
		echo "============================================"
		echo "Hardening."
		echo "============================================"
		#remove readme.html
		rm readme.html
		#debug extras
		perl -pi -e "s/define\('WP_DEBUG', false\);/define('WP_DEBUG', false);\n\/** Useful extras *\/ \nif (WP_DEBUG) { \n\tdefine('WP_DEBUG_LOG', true); \n\tdefine('WP_DEBUG_DISPLAY', false); \n\t\@ini_set('display_errors',0);\n}/" wp-config.php
		# key access to mods
			find="/* That's all, stop editing! Happy blogging. */"
			replace="/** Disallow theme and plugin editor in admin. Updates only with query var */\ndefine( 'DISALLOW_FILE_EDIT', true );\nif ( \\$\_REQUEST['key'] == '$hardenkey' ) {\n\tsetcookie( 'updatebypass', 1 );\n} elseif ( ! \\$\_COOKIE['updatebypass'] ) {\n\tdefine( 'DISALLOW_FILE_MODS', true );\n}\n\n/* That's all, stop editing! Happy blogging. */"
		perl -pi -e "s{\Q$find\E}{$replace}" wp-config.php
		#create root .htaccess with some useful starters
		cat > .htaccess <<'EOL'
            # Protect this file
            <Files ~ "^\.ht">
            Order allow,deny
            Deny from all
            </Files>
            # Prevent directory listing
            Options -Indexes
            ## BEGIN 6G Firewall from https://perishablepress.com/6g/
            # 6G:[QUERY STRINGS]
            <IfModule mod_rewrite.c>
                RewriteEngine On
                RewriteCond %{QUERY_STRING} (eval\() [NC,OR]
                RewriteCond %{QUERY_STRING} (127\.0\.0\.1) [NC,OR]
                RewriteCond %{QUERY_STRING} ([a-z0-9]{2000}) [NC,OR]
                RewriteCond %{QUERY_STRING} (javascript:)(.*)(;) [NC,OR]
                RewriteCond %{QUERY_STRING} (base64_encode)(.*)(\() [NC,OR]
                RewriteCond %{QUERY_STRING} (GLOBALS|REQUEST)(=|\[|%) [NC,OR]
                RewriteCond %{QUERY_STRING} (<|%3C)(.*)script(.*)(>|%3) [NC,OR]
                RewriteCond %{QUERY_STRING} (\\|\.\.\.|\.\./|~|`|<|>|\|) [NC,OR]
                RewriteCond %{QUERY_STRING} (boot\.ini|etc/passwd|self/environ) [NC,OR]
                RewriteCond %{QUERY_STRING} (thumbs?(_editor|open)?|tim(thumb)?)\.php [NC,OR]
                RewriteCond %{QUERY_STRING} (\'|\")(.*)(drop|insert|md5|select|union) [NC]
                RewriteRule .* - [F]
            </IfModule>
            # 6G:[REQUEST METHOD]
            <IfModule mod_rewrite.c>
                RewriteCond %{REQUEST_METHOD} ^(connect|debug|delete|move|put|trace|track) [NC]
                RewriteRule .* - [F]
            </IfModule>
            # 6G:[REFERRERS]
            <IfModule mod_rewrite.c>
                RewriteCond %{HTTP_REFERER} ([a-z0-9]{2000}) [NC,OR]
                RewriteCond %{HTTP_REFERER} (semalt.com|todaperfeita) [NC]
                RewriteRule .* - [F]
            </IfModule>
            # 6G:[REQUEST STRINGS]
            <IfModule mod_alias.c>
                RedirectMatch 403 (?i)([a-z0-9]{2000})
                RedirectMatch 403 (?i)(https?|ftp|php):/
                RedirectMatch 403 (?i)(base64_encode)(.*)(\()
                RedirectMatch 403 (?i)(=\\\'|=\\%27|/\\\'/?)\.
                RedirectMatch 403 (?i)/(\$(\&)?|\*|\"|\.|,|&|&amp;?)/?$
                RedirectMatch 403 (?i)(\{0\}|\(/\(|\.\.\.|\+\+\+|\\\"\\\")
                RedirectMatch 403 (?i)(~|`|<|>|:|;|,|%|\\|\s|\{|\}|\[|\]|\|)
                RedirectMatch 403 (?i)/(=|\$&|_mm|cgi-|etc/passwd|muieblack)
                RedirectMatch 403 (?i)(&pws=0|_vti_|\(null\)|\{\$itemURL\}|echo(.*)kae|etc/passwd|eval\(|self/environ)
                RedirectMatch 403 (?i)\.(aspx?|bash|bak?|cfg|cgi|dll|exe|git|hg|ini|jsp|log|mdb|out|sql|svn|swp|tar|rar|rdf)$
                RedirectMatch 403 (?i)/(^$|(wp-)?config|mobiquo|phpinfo|shell|sqlpatch|thumb|thumb_editor|thumbopen|timthumb|webshell)\.php
            </IfModule>
            # 6G:[USER AGENTS]
            <IfModule mod_setenvif.c>
                SetEnvIfNoCase User-Agent ([a-z0-9]{2000}) bad_bot
                SetEnvIfNoCase User-Agent (archive.org|binlar|casper|checkpriv|choppy|clshttp|cmsworld|diavol|dotbot|extract|feedfinder|flicky|g00g1e|harvest|heritrix|httrack|kmccrew|loader|miner|nikto|nutch|planetwork|postrank|purebot|pycurl|python|seekerspider|siclab|skygrid|sqlmap|sucker|turnit|vikspider|winhttp|xxxyy|youda|zmeu|zune) bad_bot
                <limit GET POST PUT>
                    Order Allow,Deny
                    Allow from All
                    Deny from env=bad_bot
                </limit>
            </IfModule>
            # 6G:[BAD IPS]
            <Limit GET HEAD OPTIONS POST PUT>
                Order Allow,Deny
                Allow from All
                # uncomment/edit/repeat next line to block IPs
                # Deny from 123.456.789
            </Limit>
            ## END 6G Firewall
            ## BEGIN htauth basic authentication
            # STAGING
            Require all denied
            AuthType Basic
            AuthUserFile /etc/apache2/wp-login
            AuthName "Please Authenticate"
            Require valid-user
            # LIVE - prevent wp-login brute force attacks from causing load
            #<FilesMatch "^(wp-login|xmlrpc)\.php$">
            #	AuthType Basic
            #	AuthUserFile /etc/apache2/wp-login
            #	AuthName "Please Authenticate"
            #	Require valid-user
            #</FilesMatch>
            # Exclude the file upload and WP CRON scripts from authentication
            #<FilesMatch "(async-upload\.php|wp-cron\.php)$">
            #	Satisfy Any
            #	Order allow,deny
            #	Allow from all
            #	Deny from none
            #</FilesMatch>
            ## END htauth
            ## BEGIN WP file protection
            <Files wp-config.php>
                order allow,deny
                deny from all
            </Files>
            # WP includes directories
            <IfModule mod_rewrite.c>
                RewriteEngine On
                RewriteBase /
                RewriteRule ^wp-admin/includes/ - [F,L]
                RewriteRule !^wp-includes/ - [S=3]
                # note - comment out next line on multisite
                RewriteRule ^wp-includes/[^/]+\.php$ - [F,L]
                RewriteRule ^wp-includes/js/tinymce/langs/.+\.php - [F,L]
                RewriteRule ^wp-includes/theme-compat/ - [F,L]
            </IfModule>
            ## END WP file protection
            # Prevent author enumeration
            RewriteCond %{REQUEST_URI} !^/wp-admin [NC]
            RewriteCond %{QUERY_STRING} author=\d
            RewriteRule ^ /? [L,R=301]
            EOL
                    #create .htaccess to protect uploads directory
                    cat > wp-content/uploads/.htaccess <<'EOL'
            # Protect this file
            <Files .htaccess>
            Order Deny,Allow
            Deny from All
            </Files>
            # whitelist file extensions to prevent executables being
            # accessed if they get uploaded
            order deny,allow
            deny from all
            <Files ~ ".(docx?|xlsx?|pptx?|txt|pdf|xml|css|jpe?g|png|gif)$">
            allow from all
            </Files>
EOL
	fi
	echo "Cleaning..."
	#remove wordpress/ dir
	rmdir wordpress
	#remove zip file
	rm latest.tar.gz
	#remove bash script if it exists in this dir
	[[ -f "wp.sh" ]] && rm "wp.sh"

    wp core install --url=$ProjectDomain --title=Example --admin_user=supervisor --admin_password=123 --admin_email=info@$ProjectDomain --allow-root
    xdg-open "https://$ProjectDomain"
	echo "========================="
	echo "[Success]: Installation is complete."
	echo "========================="
else
	exit
fi


# TODO
# 1. Check Permissions
# 2. Max Execution Time
# 3. Max Upload Size
# 4. WP CLI Auto Install
# Basic Setup- Title,Tagline, URL Rewrite,
# Wordmove Config
# Plugins - Limit Login Attempts, Caching Plugin, Yoast,
# Analytics, ACF PRO, Adminize, SVG Support,wp mail smtp, WP Migrate DB, Caldera Forms, Code Snippets
# Duplicate Post
# Theme - Delete Existing Themes, Install Divi, Add API Key, Update Divi
# Pages- Home, About Us, Contact Us, Privacy Policy (All Published)
# Menu - Above PAges